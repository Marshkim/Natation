﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProjetTransverse.Controllers
{
    public class TourController : ApiController
    {
        MySqlConnection conn = WebApiConfig.conn();

        [Route("api/competition/{idCompetition}/tours")]
        [HttpGet]
        public string GetToursCompetition(int idCompetition)
        {
            MySqlCommand query = conn.CreateCommand();
            WebApiConfig.open_conn(conn);
            query.CommandText = "SELECT idtour, nom FROM competition NATURAL JOIN Tour NATURAL JOIN typetour WHERE idCompetition = " + idCompetition;
            MySqlDataReader res = query.ExecuteReader();
            List<Object> tours = new List<Object>();
            while (res.Read())
            {
                tours.Add(new
                {
                    id = res["idtour"],
                    type = res["nom"]
                });
            }
            return JsonConvert.SerializeObject(tours);
        }
    }
}
