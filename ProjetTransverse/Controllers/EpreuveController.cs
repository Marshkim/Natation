﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProjetTransverse.Controllers
{
    public class EpreuveController : ApiController
    {
        MySqlConnection conn = WebApiConfig.conn();

        [Route("api/tour/{idTour}/epreuves")]
        [HttpGet]
        public string GetEpreuvesTour(int idTour)
        {
            MySqlCommand query = conn.CreateCommand();
            WebApiConfig.open_conn(conn);
            query.CommandText = "SELECT idEpreuve, nom FROM epreuve NATURAL JOIN typeepreuve NATURAL JOIN tour WHERE idTour = " + idTour;
            MySqlDataReader res = query.ExecuteReader();
            List<Object> epreuves = new List<Object>();
            while (res.Read())
            {
                epreuves.Add(new
                {
                    id = res["idEpreuve"],
                    type = res["nom"]
                });
            }
            return JsonConvert.SerializeObject(epreuves);
        }


        [Route("api/epreuve/{idEpreuve}/{isImpose}/{typeJuge}")]
        [HttpGet]
        public string GetFigureElementByEpreuve(int idEpreuve, bool isImpose, string typeJuge)
        {
            MySqlCommand query = conn.CreateCommand();
            WebApiConfig.open_conn(conn);
            if (typeJuge == "element")
            {
                query.CommandText = "SELECT B.idEquipe, F.* FROM Figure F " +
                    "NATURAL JOIN Ballet B " +
                    "NATURAL JOIN TypeFigure TF " +
                    "WHERE TF.nom NOT LIKE 'BALLET%' AND B.idEpreuve = " + idEpreuve +
                    " ORDER BY B.idEquipe, F.idFigure";
            }else if(typeJuge == "difficulte")
            {
                query.CommandText = "SELECT B.idEquipe, F.* FROM Figure F " +
                    "NATURAL JOIN Ballet B " +
                    "NATURAL JOIN TypeFigure TF " +
                    "WHERE TF.nom = 'BALLET LIBRE' AND B.idEpreuve = " + idEpreuve +
                    " ORDER BY B.idEquipe, F.idFigure";
            }
            else
            {
                if (isImpose)
                {
                    query.CommandText = "SELECT B.idEquipe, F.* FROM Figure F " +
                        "NATURAL JOIN Ballet B " +
                        "NATURAL JOIN TypeFigure TF " +
                        "WHERE TF.nom LIKE 'BALLET COMPLET' AND B.idEpreuve = " + idEpreuve +
                        " ORDER BY B.idEquipe, F.idFigure";
                }
                else
                {
                    query.CommandText = "SELECT B.idEquipe, F.* FROM Figure F " +
                    "NATURAL JOIN Ballet B " +
                    "NATURAL JOIN TypeFigure TF " +
                    "WHERE TF.nom LIKE 'BALLET LIBRE' AND B.idEpreuve = " + idEpreuve +
                    " ORDER BY B.idEquipe, F.idFigure";
                }
            }
            MySqlDataReader res = query.ExecuteReader();
            List<int> idEquipes = new List<int>();
            Dictionary<int, List<int>> dico = new Dictionary<int, List<int>>();
            List<Object> resultats = new List<Object>();
            while (res.Read())
            {
                int idEquipe = Convert.ToInt32(res["idEquipe"]);
                int idFigure = Convert.ToInt32(res["idFigure"]);
                if (!idEquipes.Contains(Convert.ToInt32(res["idEquipe"])))
                {       
                    idEquipes.Add(idEquipe);
                    dico.Add(idEquipe, new List<int>());
                }
                dico.Where(x => x.Key == idEquipe).FirstOrDefault().Value.Add(idFigure);
            }
            res.Close();

            foreach (var equipe in idEquipes)
            {
                List<Object> nageuses = new List<Object>();
                MySqlCommand queryNageuses = conn.CreateCommand();
                queryNageuses.CommandText = "SELECT N.nom, N.prenom, N.dateNaissance, EN.isTitulaire FROM nageuse AS N NATURAL JOIN equipenageuse AS EN NATURAL JOIN equipe AS E WHERE E.idEquipe = " + equipe;
                MySqlDataReader nag = queryNageuses.ExecuteReader();
                while (nag.Read())
                {
                    double age = Math.Floor(Convert.ToDouble((DateTime.Today - Convert.ToDateTime(nag["dateNaissance"])).Days) / 365);
                    nageuses.Add(new
                    {
                        nom = nag["nom"],
                        prenom = nag["prenom"],
                        age = Convert.ToInt32(age),
                        isTitulaire = nag["isTitulaire"]
                    });
                }
                nag.Close();
                resultats.Add(new {
                    equipe = equipe,
                    nageuses = nageuses,
                    figures = dico.Where(x => x.Key == equipe).FirstOrDefault().Value.ToArray()
                });
            }
            return JsonConvert.SerializeObject(resultats);
        }

        [Route("api/epreuve/{idEpreuve}/notes/{isImpose}")]
        [HttpGet]
        public string GetNotesEpreuve(int idEpreuve, bool isImpose)
        {
            MySqlCommand query = conn.CreateCommand();
            WebApiConfig.open_conn(conn);
            query.CommandText = "SELECT E.note, Eq.idEquipe, F.idFigure, TF.nom, J.idJuge, R.nom As role " +
            "FROM evaluer As E " +
            "NATURAL JOIN figure As F " +
            "NATURAL JOIN typefigure AS TF " +
            "NATURAL JOIN ballet As B " +
            "NATURAL JOIN equipe AS Eq " +
            "NATURAL JOIN juge AS J " +
            "NATURAL JOIN competitionjuge AS CJ " +
            "INNER JOIN role AS R ON CJ.idRole = R.idRole " +
            "WHERE B.idEpreuve = " + idEpreuve + " AND B.isImpose = " + isImpose;

            MySqlDataReader res = query.ExecuteReader();
            List<int> idEquipes = new List<int>();
            Dictionary<int, List<int>> dicoElement = new Dictionary<int, List<int>>();
            Dictionary<int, List<int>> dicoArtistique = new Dictionary<int, List<int>>();
            Dictionary<int, List<int>> dicoExecution = new Dictionary<int, List<int>>();
            Dictionary<int, List<int>> dicoDifficulte = new Dictionary<int, List<int>>();
            List<Object> resultats = new List<Object>();
            while (res.Read())
            {
                int idEquipe = Convert.ToInt32(res["idEquipe"]);
                int note = Convert.ToInt32(res["note"]);
                if (!idEquipes.Contains(Convert.ToInt32(res["idEquipe"])))
                {
                    idEquipes.Add(idEquipe);
                }
                if (res["role"].ToString() == "Element")
                {
                    if (!dicoElement.ContainsKey(idEquipe))
                    {
                        dicoElement.Add(idEquipe, new List<int>());
                    }
                    dicoElement.Where(x => x.Key == idEquipe).FirstOrDefault().Value.Add(note);
                }
                else if (res["role"].ToString() == "Artistique")
                {
                    if (!dicoArtistique.ContainsKey(idEquipe))
                    {
                        dicoArtistique.Add(idEquipe, new List<int>());
                    }
                    dicoArtistique.Where(x => x.Key == idEquipe).FirstOrDefault().Value.Add(note);
                }
                else if (res["role"].ToString() == "Execution")
                {
                    if (!dicoExecution.ContainsKey(idEquipe))
                    {
                        dicoExecution.Add(idEquipe, new List<int>());
                    }
                    dicoExecution.Where(x => x.Key == idEquipe).FirstOrDefault().Value.Add(note);
                }
                else if (res["role"].ToString() == "Difficulte")
                {
                    if (!dicoDifficulte.ContainsKey(idEquipe))
                    {
                        dicoDifficulte.Add(idEquipe, new List<int>());
                    }
                    dicoDifficulte.Where(x => x.Key == idEquipe).FirstOrDefault().Value.Add(note);
                }
            }
            res.Close();

            foreach (var equipe in idEquipes)
            {
                List<Object> nageuses = new List<Object>();
                MySqlCommand queryNageuses = conn.CreateCommand();
                queryNageuses.CommandText = "SELECT N.nom, N.prenom, N.dateNaissance, EN.isTitulaire FROM nageuse AS N NATURAL JOIN equipenageuse AS EN NATURAL JOIN equipe AS E WHERE E.idEquipe = " + equipe;
                MySqlDataReader nag = queryNageuses.ExecuteReader();
                while (nag.Read())
                {
                    double age = Math.Floor(Convert.ToDouble((DateTime.Today - Convert.ToDateTime(nag["dateNaissance"])).Days) / 365);
                    nageuses.Add(new
                    {
                        nom = nag["nom"],
                        prenom = nag["prenom"],
                        age = Convert.ToInt32(age),
                        isTitulaire = nag["isTitulaire"]
                    });
                }
                nag.Close();
                if (isImpose)
                {
                    resultats.Add(new
                    {
                        equipe = equipe,
                        nageuses = nageuses,
                        element = dicoElement.Where(x => x.Key == equipe).FirstOrDefault().Value.ToArray(),
                        artistique = dicoArtistique.Where(x => x.Key == equipe).FirstOrDefault().Value.ToArray(),
                        execution = dicoExecution.Where(x => x.Key == equipe).FirstOrDefault().Value.ToArray()
                    });
                }
                else
                {
                    resultats.Add(new
                    {
                        equipe = equipe,
                        nageuses = nageuses,
                        artistique = dicoArtistique.Where(x => x.Key == equipe).FirstOrDefault().Value.ToArray(),
                        execution = dicoExecution.Where(x => x.Key == equipe).FirstOrDefault().Value.ToArray(),
                        difficulte = dicoDifficulte.Where(x => x.Key == equipe).FirstOrDefault().Value.ToArray()
                    });
                }
            }
            return JsonConvert.SerializeObject(resultats);
        }

        [Route("api/epreuve/{idEpreuve}/equipe/{idEquipe}/notes/{isImpose}")]
        [HttpGet]
        public string GetNotesEquipeEpreuve(int idEpreuve, int idEquipe, bool isImpose)
        {
            MySqlCommand query = conn.CreateCommand();
            WebApiConfig.open_conn(conn);
            query.CommandText = "SELECT E.note, Eq.idEquipe, F.idFigure, TF.nom, J.idJuge, R.nom As role " +
            "FROM evaluer As E " +
            "NATURAL JOIN figure As F " +
            "NATURAL JOIN typefigure AS TF " +
            "NATURAL JOIN ballet As B " +
            "NATURAL JOIN equipe AS Eq " +
            "NATURAL JOIN juge AS J " +
            "NATURAL JOIN competitionjuge AS CJ " +
            "INNER JOIN role AS R ON CJ.idRole = R.idRole " +
            "WHERE B.idEpreuve = " + idEpreuve + " AND idEquipe = " + idEquipe + " AND B.isImpose = " + isImpose;

            MySqlDataReader res = query.ExecuteReader();
            List<int> notesElement = new List<int>();
            List<int> notesArtistique = new List<int>();
            List<int> notesExecution = new List<int>();
            List<int> notesDifficulte = new List<int>();
            List<Object> resultats = new List<Object>();
            while (res.Read())
            {
                int note = Convert.ToInt32(res["note"]);
                if (res["role"].ToString() == "Element")
                {
                    notesElement.Add(note);
                }
                else if (res["role"].ToString() == "Artistique")
                {
                    notesArtistique.Add(note);
                }
                else if (res["role"].ToString() == "Execution")
                {
                    notesExecution.Add(note);
                }
                else if (res["role"].ToString() == "Difficulte")
                {
                    notesDifficulte.Add(note);
                }
            }
            res.Close();

            List<Object> nageuses = new List<Object>();
            MySqlCommand queryNageuses = conn.CreateCommand();
            queryNageuses.CommandText = "SELECT N.nom, N.prenom, N.dateNaissance, EN.isTitulaire FROM nageuse AS N NATURAL JOIN equipenageuse AS EN NATURAL JOIN equipe AS E WHERE E.idEquipe = " + idEquipe;
            MySqlDataReader nag = queryNageuses.ExecuteReader();
            while (nag.Read())
            {
                double age = Math.Floor(Convert.ToDouble((DateTime.Today - Convert.ToDateTime(nag["dateNaissance"])).Days) / 365);
                nageuses.Add(new
                {
                    nom = nag["nom"],
                    prenom = nag["prenom"],
                    age = Convert.ToInt32(age),
                    isTitulaire = nag["isTitulaire"]
                });
            }
            nag.Close();
            if (isImpose)
            {
                resultats.Add(new
                {
                    equipe = idEquipe,
                    nageuses = nageuses,
                    element = notesElement,
                    artistique = notesArtistique,
                    execution = notesExecution
                });
            }
            else
            {
                resultats.Add(new
                {
                    equipe = idEquipe,
                    nageuses = nageuses,
                    artistique = notesArtistique,
                    execution = notesExecution,
                    difficulte = notesDifficulte
                });
            }
            return JsonConvert.SerializeObject(resultats);
        }
    }
}
