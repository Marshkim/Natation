﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ProjetTransverse.Controllers
{
    public class JugeController : ApiController
    {
        MySqlConnection conn = WebApiConfig.conn();

        [Route("api/juge/{id}/competitions")]
        [HttpGet]
        public string GetCompetitionsJuge(int id)
        {
            MySqlCommand query = conn.CreateCommand();
            WebApiConfig.open_conn(conn);
            query.CommandText = "SELECT idCompetition, titre, dateCompet, lieu FROM competition NATURAL JOIN juge NATURAL JOIN competitionjuge WHERE idJuge = " + id;
            MySqlDataReader res = query.ExecuteReader();
            List<Object> competitions = new List<Object>();
            while (res.Read())
            {
                competitions.Add(new
                {
                    idCompetition = res["idCompetition"],
                    titre = res["titre"],
                    dateCompet = res["dateCompet"],
                    lieu = res["lieu"]
                });
            }
            return JsonConvert.SerializeObject(competitions);
        }

        [Route("api/juge/{idJuge}/notation/{idFigure}/{note}")]
        [HttpGet]
        public int NotationJuge(int note, int idJuge, int idFigure)
        {
            MySqlCommand query = conn.CreateCommand();
            WebApiConfig.open_conn(conn);
            query.CommandText = "INSERT INTO Evaluer VALUES(" + note + ", default, " + idJuge + ", " + idFigure + ")";
            int res = query.ExecuteNonQuery();
            return res;
        }


        [Route("api/juge/{idJuge}/updatenotation/{idFigure}/{note}")]
        [HttpGet]
        public int UpdateNotationJuge(int note, int idJuge, int idFigure)
        {
            MySqlCommand query = conn.CreateCommand();
            WebApiConfig.open_conn(conn);
            query.CommandText = "UPDATE Evaluer SET note = " + note + " WHERE idJuge = " + idJuge + " AND idFigure = " + idFigure + ";";
            int res = query.ExecuteNonQuery();
            return res;
        }

    }
}
