﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using ProjetTransverse.Models;

namespace ProjetTransverse.Controllers
{
    public class CompetitionController : ApiController
    {
        MySqlConnection conn = WebApiConfig.conn();

        [Route("api/competitions")]
        [HttpGet]
        public string getCompetitions()
        {
            MySqlCommand query = conn.CreateCommand();
            WebApiConfig.open_conn(conn);
            query.CommandText = "SELECT * FROM competition";
            MySqlDataReader res = query.ExecuteReader();
            List<Object> competitions = new List<Object>();
            while (res.Read())
            {
                competitions.Add(new {
                    idCompetition = res["idCompetition"],
                    titre = res["titre"],
                    dateCompet = res["dateCompet"],
                    lieu = res["lieu"]
                });
            }
            return JsonConvert.SerializeObject(competitions);
        }

        [Route("api/competition/{idCompetition}")]
        public string Get(int idCompetition)
        {
            MySqlCommand query = conn.CreateCommand();
            WebApiConfig.open_conn(conn);
            query.CommandText = "SELECT * FROM competition WHERE idCompetition = " + idCompetition;
            MySqlDataReader res = query.ExecuteReader();
            List<Object> competitions = new List<Object>();
            while (res.Read())
            {
                competitions.Add(new
                {
                    idCompetition = res["idCompetition"],
                    titre = res["titre"],
                    dateCompet = res["dateCompet"],
                    lieu = res["lieu"]
                });
            }
            return JsonConvert.SerializeObject(competitions);
        }
    }
}