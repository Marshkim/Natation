﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ProjetTransverse.Models
{
    public class Categorie
    {
        [Key]
        public Int32 idCategorie { get; set; }
        [Required]
        public String nom { get; set; }
    }
}