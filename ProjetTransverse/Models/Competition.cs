﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace ProjetTransverse.Models
{
    public class Competition
    {
        [Key]
        public Int32 id { get; set; }
        [Required]
        public String titre { get; set; }
        public DateTime dateCompet { get; set; }
        public String lieu { get; set; }
        // Foreign key
        public Int32 idCategorie { get; set; }
        // Navigation property
        public Categorie Categorie { get; set; }

        public Competition(Int32 id, String titre, DateTime dateCompet, String lieu)
        {
            this.id = id;
            this.titre = titre;
            this.dateCompet = dateCompet;
            this.lieu = lieu;
        }

        public Competition() { }
    }
}